(function ($) {
  Drupal.behaviors.commerceShift4 = {

    attach:function (context) {

      if ($('#edit-commerce-payment-payment-method-shift4-i4gocommerce-payment-shift4-i4go').is(':checked')) {
        var processed_card = false;
        // Attach a submit event handler to the checkout form.
        $("#commerce-checkout-form-review").submit(event, function(event) {
          if (!processed_card) {
            event.preventDefault();
            var settings = Drupal.settings.commerceShift4;

            var card = $('.form-item-commerce-payment-payment-details-credit-card-number input').val();
            var cvv2 = $('.form-item-commerce-payment-payment-details-credit-card-code input').val();
            var exp_month = $('.form-item-commerce-payment-payment-details-credit-card-exp-month select').val();
            var exp_year = $('.form-item-commerce-payment-payment-details-credit-card-exp-year select').val();

            var data = {
              fuseaction : 'account.jsonpPostCardEntry',
              i4Go_CardNumber: card,
              i4Go_CVV2Code: cvv2,
              i4Go_ExpirationMonth: exp_month,
              i4Go_ExpirationYear: exp_year,
              i4Go_Server : settings.i4goServer,
              i4Go_AccessBlock : settings.i4goAccessBlock
            }
            // Execute tokenization request via JSONP.
            $.ajax({
              type: 'get',
              url: settings.i4goServer,
              data: data,
              dataType: 'jsonp',
              jsonpCallback: 'parseResponse',
              success: function(response) {
                if (response.i4go_responsecode == 1) {
                  // Set token and response code fields, the server will do all
                  // validation.
                  $('input[name="commerce_payment[payment_details][unique_id]"]').val(response.i4go_uniqueid);
                  $('input[name="commerce_payment[payment_details][response_code]"]').val(response.i4go_responsecode);
                  $('input[name="commerce_payment[payment_details][card_type]"]').val(response.i4go_cardtype);

                  processed_card = true;
                  $("#commerce-checkout-form-review").submit();
                }
                else {
                  alert("There was a problem processing your card. Please try again or contact a Site Administrator");
                  location.reload();
                }
              },
            });
          }
        });
      }
    }
  }
})(jQuery);
