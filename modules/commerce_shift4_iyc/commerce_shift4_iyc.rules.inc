<?php

/**
 * @file
 * Rules integration with Commerce for Shift4 IYC service.
 */

/**
 * Implements hook_rules_condition_info().
 */
function commerce_shift4_iyc_rules_condition_info() {
  $conditions = array();

  $conditions['commerce_shift4_iyc_balance_not_paid'] = array(
    'label' => t('Order balance is not paid'),
    'parameter' => array(
      'commerce_order' => array(
        'type' => 'commerce_order',
        'label' => t('Order'),
      ),
    ),
    'group' => t('Commerce Payment'),
  );

  return $conditions;
}

/**
 * Condition callback: checks if a balance has been paid in full already.
 */
function commerce_shift4_iyc_balance_not_paid($order) {
  $bal = commerce_payment_order_balance($order);
  if ($bal['amount'] <= 0) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}