(function ($) {
  Drupal.behaviors.commerceShift4IYC = {

    attach:function (context) {

      var processed_giftcard = false;

      // Attach a submit event handler to the checkout form.
      $("#commerce-checkout-form-checkout").submit(event, function(event) {

        var card = $('.form-item-commerce-shift4-iyc-form-gift-card input').val();

        if (!processed_giftcard && card.length != 0) {
          event.preventDefault();

          $("#edit-commerce-shift4-iyc-form-gift-card").data("processed", false);
          var settings = Drupal.settings.commerceShift4IYC;

          var data  = {
            fuseaction : 'account.jsonpPostCardEntry',
            i4Go_CardNumber: card,
            i4Go_ExpirationMonth: 12,
            i4Go_ExpirationYear: 2018,
            i4Go_Server : settings.i4goServer,
            i4Go_AccessBlock : settings.i4goAccessBlock
          }

          // Execute tokenization request via JSONP.
          $.ajax({
            type: 'get',
            url: settings.i4goServer,
            data: data,
            dataType: 'jsonp',
            jsonpCallback: 'parseResponse',
            success: function(response) {
              if (response.i4go_responsecode == 1) {
                // Set token and response code fields, the server will do all
                // validation.
                $('input[name="commerce_shift4_iyc_form[unique_id]"]').val(response.i4go_uniqueid);
                $('input[name="commerce_shift4_iyc_form[response_code]"]').val(response.i4go_responsecode);
                $('input[name="commerce_shift4_iyc_form[card_type]"]').val(response.i4go_cardtype);

                processed_giftcard = true;
                $("#commerce-checkout-form-checkout").submit();
              }
              else {
                alert("There was a problem processing your gift card. Please try again or contact a Site Administrator");
                location.reload();
              }
            },
          });
        }
      });
    }
  }
})(jQuery);
