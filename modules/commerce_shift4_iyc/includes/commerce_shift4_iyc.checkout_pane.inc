<?php

/**
 * Checkout pane callback: Creates the gift card input form elements. Note we're
 * doing exactly what the shift4 module does just not treating it as a payment
 * method.
 */
function commerce_shift4_iyc_pane_checkout_form($form, &$form_state, $checkout_pane, $order) {
  $payment_method = commerce_payment_method_instance_load('shift4_i4go|commerce_payment_shift4_i4go');

  // Authorize customer. If successful, we get back a shift4 URL that we can
  // post to and we can build the gift card form.
  $auth_reponse = commerce_shift4_i4go_authorize_customer($payment_method);
  if ($auth_reponse['i4go_response'] == 'SUCCESS') {
    $settings = array(
      'i4goServer' => $auth_reponse['i4go_server'],
      'i4goAccessBlock' => $auth_reponse['i4go_accessblock']
    );

    // Add server and accessblock as JS settings.
    drupal_add_js(array('commerceShift4IYC' => $settings), 'setting');

    // Store the payment method for validate and submit handlers
    $pane_form['#iyc_vars']['#payment_method'] = $payment_method;

    // Prepare hidden values to hold response code and token.
    $pane_form['unique_id'] = array(
      '#type' => 'hidden',
      '#default_value' => ''
    );

    $pane_form['card_type'] = array(
      '#type' => 'hidden',
      '#default_value' => ''
    );

    $pane_form['response_code'] = array(
      '#type' => 'hidden',
      '#default_value' => ''
    );

    // Attach gift card form.
    $pane_form['gift_card'] = array(
      '#type' => 'textfield',
      '#title' => t('Gift Card Number'),
      '#size' => 25,
    );

    return $pane_form;
  }

}

/**
 * Validates the gift card number and sets the transaction amount.
 *
 * Note we also try to get an approved transaction here so that we can fail
 * validation if for some reason we're unable to.
 */
function commerce_shift4_iyc_pane_checkout_form_validate($form, &$form_state, $checkout_pane, $order) {
  if (empty($form_state['values']['commerce_shift4_iyc_form']['gift_card'])) {
    // We don't care about empty card values.
    return TRUE;
  }

  // We do a balance inquiry to either a) fail validation if the card has no
  // funds, or b) set the transaction amount. Balance inquiry is FRC 61.
  $payment_method = $form['commerce_shift4_iyc_form']['#iyc_vars']['#payment_method'];
  $data = array(
    'payment_method' => $payment_method,
    'order' => $order,
    'response_xml' => NULL,
    'primary_amount' => 0,
  );

  $data['pane_values'] = array(
    'unique_id' => $form_state['values']['commerce_shift4_iyc_form']['unique_id'],
    'card_type' => $form_state['values']['commerce_shift4_iyc_form']['card_type'],
  );

  $xml = commerce_shift4_request_manager('61', $data);
  if (!$xml) {
    drupal_set_message(t('There was a problem processing your gift card. Please try again.'), 'error');
    return FALSE;
  }
  if ($xml->iycavailablebalance <= 0) {
    drupal_set_message(t('There are no remaining funds on this gift card.'), 'error');
    return FALSE;
  }

  // Determine the transaction amount. When comparing we need to remember that
  // the response balance is in dollars and our order total is in cents.
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  // Get the card balance in cents.
  $currency_code = $order_wrapper->commerce_order_total->currency_code->value();
  $gc_bal = commerce_currency_decimal_to_amount($xml->iycavailablebalance, $currency_code);
  if ($gc_bal >= $order_wrapper->commerce_order_total->amount->value()) {
    $data['primary_amount'] = commerce_currency_amount_to_decimal($order_wrapper->commerce_order_total->amount->value(), $currency_code);
  }
  else {
    $data['primary_amount'] = commerce_currency_amount_to_decimal($gc_bal, $currency_code);
  }

  // Try to add a payment transaction to the order.
  // Manage the request.
  $xml = commerce_shift4_request_manager('1B', $data);
  if (!$xml) {
    drupal_set_message(t('There was a problem processing your gift card. Please try again.'), 'error');
    return FALSE;
  }

  // The request has been approved. See if we have any other reason to void.
  $data['response_xml'] = $xml;
  switch ($xml->response) {
    case 'D':
    // The txn is declined. Void it.
    case 'R':
      // The txn requires a voice referral. Assuming it's not supported so we
      // void.
      return commerce_shift4_request_manager('08', $data);
    case 'e':
    // It's a special error case coming from an FRC 07. Log it.
      commerce_shift4_error_response('commerce_shift4 Timeout Error Response', $xml);
      return FALSE;
    default:
      // The transaction is approved.
      $tran_status = COMMERCE_PAYMENT_STATUS_PENDING;
      break;
  }

  // Create a new transaction.
  $transaction = commerce_payment_transaction_new('shift4_i4go', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->remote_id = $data['response_xml']->uniqueid;
  $transaction->amount = commerce_currency_decimal_to_amount($data['primary_amount'], $currency_code);
  $transaction->currency_code = $currency_code;
  $transaction->status = $tran_status;
  $transaction->payload[REQUEST_TIME] = $data['response_xml']->asXML();
  $transaction->message = 'Number: @number';
  $transaction->message_variables = array(
    '@number' => (string) $data['response_xml']->cardnumber,
  );
  commerce_payment_transaction_save($transaction);

  return TRUE;
}
